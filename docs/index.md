# Documentation How-to Guide

This documentation helps users create and manage their documentation/static sites based on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

1. [Create a new documentation/static site](gitlabpagessite/create_site/index.md).
2. [Modify your site's settings](gitlabpagessite/modify_site_settings.md).
3. [Review before deploying](gitlabpagessite/review/index.md).

Make sure to also have a look at the [advanced topics](advanced/index.md) for more advanced how-to guides.

## Contact

In case you have a request not covered by the documentation please get in touch by creating a [SNOW Request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=gitlab-pages).

If you want to report an issue please, please [create an incident](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&se=gitlab-pages).

GitLab Pages sites are subject to the [Privacy Notice for Web Hosting Services](https://cern.service-now.com/service-portal?id=privacy_policy&se=PaaS-Web-App&notice=web-hosting).