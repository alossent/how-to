# Publish MkDocs pages from multiple git repositories to a single GitLab Pages domain

It is possible to publish your MkDocs pages from multiple git repositories to a single GitLab Pages domain by following the steps below:

#### 1. Create your MkDocs content

For example:

- Site 1: <https://gitlab.cern.ch/authoring/documentation/mkdocs-example-1-multiple-repositories>
- Site 2: <https://gitlab.cern.ch/authoring/documentation/mkdocs-example-2-multiple-repositories>

The repositories contain:

- a `docs/` directory with the content of the site
- a `mkdocs.yaml` file in which it is **necessary** to define the `nav` configuration field:

```yml
# Example
...
nav:
  - Introduction: index.md
  - 'Chapter 1':
    - 'Chapter 1': chapter1/file.md
    - 'Chapter 1.1':
      - 'Chapter 1.1': chapter1/subchapter1/subfile.md
      - 'Chapter 1.1.1':
        - 'Chapter 1.1.1': chapter1/subchapter1/subchapter1a/file1a.md
        - 'Chapter 1.1.2': chapter1/subchapter1/subchapter1a/filewithimage.md
  - 'Chapter 2': chapter2/index.md
```

The `nav` field is required by the [mkdocs-merge](https://github.com/ovasquez/mkdocs-merge) plugin which will merge the navigation of all sites in the *central* repository.

- a `.gitlab-ci.yml` with a definition of a job responsible for triggering a pipeline (in the *central* repository) rebuilding and redeploying new changes:

```bash
stages:
  - trigger

trigger-job:
  stage: trigger
  trigger:
    # Central repository serving the GitLab pages site
    project: authoring/documentation/mkdocs-example-central-multiple-repositories
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
```

#### 2. Create a central repository

Then, you need to create a *central* repository e.g. [MkDocs Example - Central - Multiple repositories](https://gitlab.cern.ch/authoring/documentation/mkdocs-example-central-multiple-repositories).

The repository contains:

- a `docs/` directory with the content of the site
- a [`mkdocs.yaml`](https://gitlab.cern.ch/authoring/documentation/mkdocs-example-central-multiple-repositories/-/blob/master/mkdocs.yml#L21) file in which it is necessary to define the `nav` configuration field
- a `.gitlab-ci.yml` responsible for cloning, merging and publishing the site's content. In the [.gitlab-ci.yml](https://gitlab.cern.ch/authoring/documentation/mkdocs-example-central-multiple-repositories/-/blob/master/.gitlab-ci.yml), you need to specify which repositories need to be cloned in the `mkdocs-merge` job:
```bash
...
  script:
    # Clone repositories
    # Example:
    # - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.example.com/<namespace>/<project>
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/authoring/documentation/mkdocs-example-1-multiple-repositories.git
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/authoring/documentation/mkdocs-example-2-multiple-repositories.git
...
```

!!! Note
    Make sure that users pushing to each repository (site 1, site 2 etc.):

    - have permissions to run pipelines in the *central* repo i.e. Developer access level or more 
    - have permissions to read (clone) the repo 2 (and each other merged repo)

    This is needed because the `CI_JOB_TOKEN` which is passed to the downstream pipeline, 
    and used to clone the other repos, holds the same permissions as the user who triggered the initial job.

    In addition, users triggering pipelines in the `central` repository need to have permissions to read (clone) each merged repo.

#### 3. [Create a new project on Web Services](../gitlabpagessite/create_site/create_webeos_project.md)

#### 4. [Set up GitLab Pages](../gitlabpagessite/create_site/set_up_pages.md) on the *central* repository
