# Check Markdown syntax and page appearance

This is the most simple alternative, but very useful if your goal is to check your Markdown syntax and how a single page of your site would look like in terms of content structure, etc. In order to do this, you can simply create a new note in [CERN's deployment of CodiMD](https://codimd.web.cern.ch){target=_blank} and use the preview to see how the final HTML might look.

!!! warning
    Notice that it is not 100% reliable because MkDocs has different plugins and uses slightly different MarkDown flavour, so this will be useful just for checking the approximate look of the tested page.
